<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/site.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" id="camera-css"  href="css/camera.css" type="text/css" media="all">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
<!--Title-->
<title>CAVASOFT | Inicio</title>
</head>
<body>
  <div class="container box_shadow">
    <!-- MENU -->
    <jsp:include page="menu.jsp">
      <jsp:param name="current" value="welcome"/>
    </jsp:include>

    <div class="page_container">
			<div class="container">
				<!--slider-->
				<div id="main_slider">
					<div class="camera_wrap" id="camera_wrap_1">
						<div data-src="images/slider/slide1.jpg">
							<div class="camera_caption fadeIn">
								<div class="slide_descr">Manage and Measure <br/>Your Marketing Goals</div>
							</div>
						</div>
						<div data-src="images/slider/slide2.jpg">
							<div class="camera_caption fadeIn">
								<div class="slide_descr">Expertise You Can Count On<br/>Results You Can Measure</div>
							</div>
						</div>
						<div data-src="images/slider/slide3.jpg">
							<div class="camera_caption2 fadeIn">
								<div class="slide_descr">Your Lead-Generation Partner<br/>With a Vision</div>
							</div>
						</div>
					</div><!-- #camera_wrap_1 -->
					<div class="clear"></div>
				</div>
				<!--//slider-->
			</div>
      <div class="wrap planning">
				<div class="fist_line_planning">
					<a href="javascript:void(0);">
						<span class="color1 service_block">
							<img class="icon_block" src="images/icon1.png" />
                            <span class="link_title">PLAFT-sw</span>
							<span class="service_txt">Our Strategy Team converts brand values into actionable brand behavior. We implement and measure e-business strategies to provide maximum exposure to consumers everywhere around the globe. Our strategy makes our clients thrive. </span>
						</span>
					</a>
					<a href="javascript:void(0);">
						<span class="color2 service_block">
							<img class="icon_block" src="images/icon2.png" />
							<span class="link_title">ACOMEXT</span>
							<span class="service_txt"> With mobile search, your ads will display through search results performed on cell phones in order to expand your reach. By allowing your ads to travel wherever your customers go, the potential to promote your business is available anytime, anywhere.</span>
						</span>
					</a>
					<a href="javascript:void(0);">
						<span class="color3 service_block">
							<img class="icon_block" src="images/icon3.png" />
							<span class="link_title">Social Media</span>
							<span class="service_txt"> Being a social media marketing agency, we have developed a digital strategy to maximize your organization's engagement and benefit from this exciting and powerful communication platform, while complementing other digital marketing initiatives.</span>
						</span>
					</a>
					<div class="clear"></div>
				</div>
			</div>
    </div>

    <!--  FOOTER  -->
    <jsp:include page="footer.jsp" />
  </div>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
    <script type="text/javascript" src="js/camera.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>

</body>
</html>
