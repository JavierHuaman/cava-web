<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
<meta charset="utf-8">
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/site.css" rel="stylesheet">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
<!--Title-->
<title>CAVASOFT | Lavado de Activos</title>
<!--</head>-->
<body>
  <div class="container box_shadow">
    <jsp:include page="menu.jsp">
      <jsp:param name="current" value="acomext"/>
    </jsp:include>
    <!--page_container-->
    <div class="page_container">
    	<div class="wrap">
        <div class="breadcrumb">
				  <div>
					  <a href="welcome.jsp">Inicio</a><span>/</span>Comercio Exterior
				  </div>
			  </div>
			  <div class="container">
          <section>
            Pagina en construcción .
          </section>
        </div>
      </div>
    </div>
    <!--//page_container-->
		<jsp:include page="footer.jsp"/>
	</div>
  <script src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script type="text/javascript" src="js/superfish.js"></script>
  <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
