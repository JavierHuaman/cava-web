<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="utf-8">
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/site.css" rel="stylesheet">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
<!--Title-->
<title>CAVASOFT | Acerca de Nosotros</title>
</head>
<body>
  <div class="container box_shadow">
    <!-- MENU -->
    <jsp:include page="menu.jsp">
      <jsp:param name="current" value="about"/>
    </jsp:include>

    <div class="page_container">
    	<div class="wrap">
        <div class="breadcrumb">
				  <div>
					  <a href="welcome.jsp">Inicio</a><span>/</span>Acerca de Nosotros
				  </div>
			  </div>
			  <div class="container">
          <section>
            <div class="row">
              <div class="span4">
                  <h2 class="title">Nuestra Historia</h2>
                  <p>By driving leads and working to find solutions for our clients, AmazingBiz has grown to become one of the largest and most successful digital media agencies. Founded in 2000, we are servicing more than 150 clients worldwide.</p> <p>Our decades of experience in connecting buyers is the basis for our future success. We know where to find the leads and convert them to revenue streams.</p>
              </div>
              <div class="span4">
                  <h2 class="title">Nuestra mision</h2>
              	<p>Evolution is part of our mission and culture. It is also the driving engine of our market. That means embracing new technologies and expanding your horizons. Because true innovation can only happen when you have the curiosity to explore new ideas and the courage to discover better solutions.</p>
<p>We are always looking for a better way to deliver qualified prospects to your business.</p>

</div>
              <div class="span4">
                  <h2 class="title">Lo que nos hace diferente</h2>
              	<p>We are marketing scientists. Everything we do is based on the strongest possible foundation. Our work is defined by measurable objectives and proven techniques, and our team helps pilot programs that lead to a positive business impact for our clients.</p>

<p>And like the finest scientists, we never guess. Particularly when it comes to your business.</p>
                                      </div>
          </div>
           </section>
           <section>
                	<h2 class="title">Our Team</h2>
                    <p>We are proud of our company, not only for its unqiue identity, but because of the people that drive it! Listed here are the most seasoned members of the AmazingBiz team.</p>
                	<div class="row">
                        <div class="span3">
                            <div class="profile">
                            	<img alt="" src="images/team1.jpg">
                                <h4>Bon Makliejii</h4>
                                <div class="profile_title">Market Analyst</div>
                                <p>Bob is a recognized leader in applying database and direct marketing to branded products using traditional and electronic direct marketing.</p>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="profile">
                            	<img alt="" src="images/team1.jpg">
                                <h4>Bill Batwanjry</h4>
                                <div class="profile_title">Marketing Consultant</div>
                                <p>Bill provides invaluable strategic guidance to our clients. He has more than 20 years of  experience in marketing planning and direct marketing.</p>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="profile">
                            	<img alt="" src="images/team1.jpg">
                                <h4>Don Parvana</h4>
                                <div class="profile_title">Corporate Relations</div>
                                <p>Don leads campaigns that help our clients explore new channels, reach untapped segments and make their marketing efforts more effective.</p>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="profile">
                            	<img alt="" src="images/team1.jpg">
                                <h4>Eric Charseela</h4>
                                <div class="profile_title">Accounts Manager</div>
                                <p>Eric is in charge of account management, developing highly successful marketing and promotional programs. </p>
                            </div>
                        </div>
                    </div>
                </section>
        </div>
			</div>
		</div>
    <!--  FOOTER  -->
    <jsp:include page="footer.jsp" />
  </div>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
