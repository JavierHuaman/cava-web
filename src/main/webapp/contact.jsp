<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
<meta charset="utf-8">
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/site.css" rel="stylesheet">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
<!--Title-->
<title>CAVASOFT | Contacto</title>
<!--</head>-->
<body>
  <div class="container box_shadow">
    <!-- MENU -->
    <jsp:include page="menu.jsp">
      <jsp:param name="current" value="contact"/>
    </jsp:include>
    <!--//MENU-->
    <!-- CONTAINER -->
    <div class="page_container">
      <div class="wrap">
        <div class="breadcrumb">
          <div> <a href="welcome.jsp">Inicio</a><span>/</span> Contacto </div>
        </div>
        <div class="container">
          <section>
            <div class="row">
              <div class="span4">
                <h2 class="title">Información de Contacto</h2>
                <p> Clle. Varsovia N 139 Of. 101 Urb. Los Sauces II
                    <br/> Surquillo - Lima, Peru
                </p>
                <p>Teléfax: (01) 449-6929 <br/> Celular: 94977-8478
                   <br/> Email: <a>cesarvargas@cavasoftsac.com</a>
                </p>
              </div>
              <div class="span8">
                <h2 class="title">Estar en contacto</h2>
                <div class="contact_form">
                  <div id="notes"></div>
                  <div id="fields">
                    <form id="contact-form">
                      <input class="span7" type="text" name="name" placeholder="Nombre (requerido)"/>
                      <input class="span7" type="text" name="email" placeholder="Correo Eléctronico (requerido)"/>
                      <input class="span7" type="text" name="subject" placeholder="Sujeto (requerido)"/>
                      <textarea class="span8" id="message" name="message"></textarea>
                      <div class="clear"></div>
                      <input type="reset" class="contact_btn" value="Limpiar formulario"/>
                      <input type="submit" class="contact_btn" value="Enviar"/>
                      <div class="clear"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
    <!-- //CONTAINER -->
    <!--  FOOTER  -->
    <jsp:include page="footer.jsp" />
  </div>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
