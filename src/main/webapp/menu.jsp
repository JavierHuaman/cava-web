<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--header-->
<div class="header">
	<div class="wrap">
		<div class="container">
			<div class="fleft logo"><a href="welcome.jsp"><img src="images/logo-cava.png" alt="Amazing Ideas" /></a></div>
			<div class="navbar fright">
			<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							<li class="first welcome">
							  <a href="welcome.jsp">Inicio</a>
							</li>
							<li class="last about">
							  <a href="about.jsp">Acerca de Nosotros</a>
							</li>
							<li class="sub-menu first plaft">
							    <a href="javascript:{}">Lavado de Activo</a>
								<ul>
									<li><a href="plaft.jsp"><span>-</span>Principal</a></li>
								</ul>
							</li>

              <li class="last acomext">
                <a href="acomext.jsp">C. Exterior</a>
							</li>
							<li class="last contact">
							  <a href="contact.jsp">Contacto</a></li>
						</ul>
					</div>
				</nav>
		</div>
			<div class="clear"></div>
		</div>

	</div>
</div>
        <!--//header-->
<script type="text/javascript">
  var clase = '<%=request.getParameter("current")%>'
  var menu = document.getElementsByClassName(clase)[0];
  menu.classList.add("current");
</script>
