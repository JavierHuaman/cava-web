<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
<meta charset="utf-8">
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="css/site.css" rel="stylesheet">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
<!--Title-->
<title>CAVASOFT | Lavado de Activos</title>
<!--</head>-->
<body>
  <div class="container box_shadow">
    <jsp:include page="menu.jsp">
      <jsp:param name="current" value="plaft"/>
    </jsp:include>
    <!--page_container-->
    <div class="page_container">
    	<div class="wrap">
        	<div class="breadcrumb">
				<div>
					<a href="welcome.jsp">Inicio</a><span>/</span>Lavado de activo
				</div>
			</div>
			<div class="container">
        <section>
        	<div class="row">
            	<div class="span8">
                	<div class="post">
                    	<h2 class="title">
                    	  <a href="javascript:{}">
                    	    Lavado de Activos y Financiamiento del Terrorismo
                    	  </a>
                    	 </h2>
                        <div class="post_info">
                        	<div class="fleft">
                            El lavado de activo y financiamiento de terrorismo
                        	</div>
                        	<div class="clear"></div>
                        </div>
                      <p>
                      La Unidad de Inteligencia Financiera del Perú – UIF Perú, es la encargada de recibir, analizar, tratar, evaluar y transmitir información para la detección del lavado de activos y/o del financiamiento del terrorismo (LA/FT), así como, implementar por parte de los sujetos obligados el sistema para detectar operaciones sospechosas.
                      </p>
                    </div>
                    <div class="post">
                        <div class="post_info">
                        	<div class="fleft">
                            Los sujetos obligados a proporcionar información UIF Perú:
                        	</div>
                        	<div class="clear"></div>
                        </div>
                   		<p>
                   		1.	Las empresas del sistema financiero y del sistema de seguros.<br/>
                      2.	Las empresas emisoras de tarjetas de crédito y/o débito.<br/>
                      3.	Las cooperativas de ahorro y crédito.<br/>
                      4.	Los fiduciarios o administradores de bienes, empresas y consorcios.<br/>
                      5.	Las sociedades agentes de bolsa, sociedades agentes de productos y sociedades intermediarias de valores.<br/>
                      6.	Las sociedades administradoras de fondos mutuos, fondos de inversión, fondos colectivos, y fondos de seguros de pensiones.<br/>
                      7.	La Bolsa de Valores, otros mecanismos centralizados de negociación e instituciones de compensación y liquidación de valores.<br/>
                      8.	La Bolsa de Productos.<br/>
                      9.	Las empresas o personas naturales dedicadas a la compra y venta de vehículos, embarcaciones y aeronaves.<br/>
                      10.	Las empresas o personas naturales dedicadas a la actividad de la construcción e inmobiliarias.<br/>
                      11.	Los casinos, sociedades de lotería y casas de juegos, incluyendo bingos, tragamonedas, hipódromos.<br/>
                      12.	Los almacenes generales de depósito.<br/>
                      13.	Las agencias de aduana.
                   		</p>
                    </div>
                    <div class="post">
                        <div class="post_info">
                        	<div class="fleft">
                            Los sujetos obligados a informar a la UIF Perú, con respecto a operaciones sospechosas y/o operaciones de acuerdo al monto que fije el reglamento
                        	</div>
                        	<div class="clear"></div>
                        </div>
                   		<p>
                        1.	La compra y venta de divisas. <br/>
                        2.	El servicio de correo y courier.<br/>
                        3.	El comercio de antigüedades.<br/>
                        4.	El comercio de joyas, metales y piedras preciosas, monedas, objetos de arte y sellos postales.<br/>
                        5.	Los préstamos y empeño.<br/>
                        6.	Las agencias de viajes y turismo, hoteles y restaurantes.<br/>
                        7.	Los Notarios Públicos.<br/>
                        8.	Los Martilleros Públicos.<br/>
                        9.	Las personas jurídicas o naturales que reciban donaciones o aportes de terceros.<br/>
                        10.	Los despachadores de operaciones de importación y exportación.<br/>
                        11.	Los servicios de cajas de seguridad y consignaciones, que serán abiertas con<br/> autorización de su titular o por mandato judicial.<br/>
                        12.	La Comisión de Lucha contra los Delitos Aduaneros.<br/>
                        13.	Laboratorios y empresas que producen y/o comercialicen insumos químicos que se utilicen para la fabricación de drogas y/o explosivos.<br/>
                        14.	Personas naturales y/o jurídicas dedicadas a la compraventa o importaciones de armas.<br/>
                        15.	Personas naturales y/o jurídicas dedicadas a la fabricación y/o comercialización de materiales explosivos.<br/>
                        16.	Gestores de intereses en la administración pública, según Ley Nº 28024.<br/>
                        17.	Empresas mineras.<br/>
                        18.	Organizaciones e instituciones públicas receptoras de fondos que no provengan del erario nacional.
                   		</p>
                    </div>
                    <div class="post">
                        <div class="post_info">
                        	<div class="fleft">
                            Sujetos obligados a llevar un Registro de Operaciones
                          </div>
                          <div class="clear"></div>
                        </div>
                      <p>
                      Deben registrar cada operación que se realice o que se haya intentado realizar que iguale o supere el monto de umbrales que establezca la UIF-Perú:
                      </p>
                      <p>
                      a)	Depósitos en efectivo: en cuenta corriente, en cuenta de ahorros, a plazo fijo y otras.<br/>
                      b)	Depósitos constituidos con títulos valores.<br/>
                      c)	Colocación de obligaciones negociables y otros títulos valores de deuda emitida por la propia entidad.<br/>
                      d)	Compraventa de títulos valores -públicos o privados- o de cuota partes de fondos comunes de inversión.<br/>
                      e)	Compraventa de metales y/o piedras preciosas.<br/>
                      f)	Compraventa en efectivo de moneda extranjera.<br/>
                      g)	Giros o transferencias emitidas y recibidas (internas y externas) cualesquiera sea la forma utilizada para cursar las operaciones y su destino (depósitos, pases, compraventa de títulos, etc.).<br/>
                      h)	Compra venta de cheques girados contra cuentas del exterior y de cheques de viajero.<br/>
                      i)	Pago de importaciones.<br/>
                      j)	Cobro de exportaciones.<br/>
                      k)	Venta de cartera de la entidad financiera a terceros.<br/>
                      l)	Servicios de amortización de préstamos.<br/>
                      m)	Cancelaciones anticipadas de préstamos.<br/>
                      n)	Constitución de fideicomisos y todo tipo de otros encargos fiduciarios.<br/>
                      o)	Compra venta de bienes y servicios.<br/>
                      p)	Operaciones a futuro pactadas con los clientes.<br/>
                      q)	Otras operaciones que se consideren de riesgo o importancia establecidas por la UIF-Perú.
                      </p>
                    </div>
                    <div class="post">
                        <div class="post_info">
                          <div class="fleft">
                           Los organismos supervisores
                          </div>
                          <div class="clear"></div>
                        </div>
                      <p>
                      Los organismos supervisores ejercerán la función de supervisión del sistema de prevención del lavado de activos y del financiamiento del terrorismo, en coordinación con la UIF-Perú, y ejercerán la función sancionadora en el ámbito de los sujetos obligados.
                      </p>
                    </div>
                    <div class="post">
                        <div class="post_info">
                        	<div class="fleft">
                            ¿Que se entiende por operaciones sospechosas e inusuales?
                        	</div>
                        	<div class="clear"></div>
                        </div>
                      <p>
                        a)	Operaciones sospechosas, aquellas de naturaleza civil, comercial o financiera que tengan una magnitud o velocidad de rotación inusual, o condiciones de complejidad inusitada o in justificada, que se presuma proceden de alguna actividad ilícita, o que, por cualquier motivo, no tengan un fundamento económico o lícito aparente; y,<br/>
                        b)	Operaciones inusuales, aquellas cuya cuantía, características y periodicidad no guardan relación con la actividad económica del cliente, salen de los parámetros de normalidad vigente en el mercado o no tienen un fundamento legal evidente.
                      </p>
                    </div>
                    <div class="post">
                        <div class="post_info">
                        	<div class="fleft">
                            ¿Quienes son los Oficiales de Enlace?
                        	</div>
                        	<div class="clear"></div>
                        </div>
                   		<p>
                   		Es la persona natural de contacto entre el organismo supervisor y la UIF-Perú, cuyas funciones son la consulta y coordinación, así como la remisión a la UIF-Perú, de los ROS
 Es designado por el titular del organismo supervisor.
                   		</p>
                      <p>
                        a) Poder Judicial <br/>
                        b) Ministerio de Transportes y Comunicaciones<br/>
                        c) Ministerio de Relaciones Exteriores<br/>
                        d) Ministerio Público<br/>
                        e) Ministerio del Interior<br/>
                        f) Ministerio de Defensa<br/>
                        g) Ministerio de Salud<br/>
                        h) Ministerio de Energía y Minas<br/>
                        i) Contraloría General de la República<br/>
                        j) Superintendencia de Banca y Seguros - SBS<br/>
                        k) Superintendencia Nacional de Administración Tributaria-SUNAT<br/>
                        l) Comisión Nacional Supervisora de Empresas y Valores - SMV<br/>
                        m) ESSALUD<br/>
                        n) Comisión de Formalización de la Propiedad Informal<br/>
                        o) Proyecto Especial de Titulación de Tierras y Catastro Rural<br/>
                        p) Registro Nacional de Identidad y Estado Civil<br/>
                        q) Dirección General de Migraciones y Naturalización.
                      </p>
                    </div>
                </div>
              	<div class="span4">
                  	<div class="sidebar">
                    </div>
                </div>
                	</div>
                </section>
            </div>
        </div>
    </div>
    <!--//page_container-->
		<jsp:include page="footer.jsp"/>
	</div>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
