<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--footer-->
	<div class="footer_bottom">
		<div class="wrap">
			<div class="container">
				<div class="fleft copyright">Cavasoft SAC &copy;
				  <%=new java.text.SimpleDateFormat("yyyy").format(new java.util.Date())%>  |
				  <a href="javascript:void(0);">Privacy Policy</a></div>
				<div class="fright follow_us">
					<ul>
						<li><a href="http://twitter.com/egrappler" class="soc1"></a></li>
						<li><a href="http://facebook.com/egrappler" class="soc2"></a></li>
						<li><a href="#" class="soc3"></a></li>
						<div class="clear"></div>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
<!--//footer-->
